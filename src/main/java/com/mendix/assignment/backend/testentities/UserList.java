package com.mendix.assignment.backend.testentities;

import com.mendix.assignment.backend.mappers.User.UserResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * Simple class created for test goals in order to wrap list of user responses returned from the rest template in cucumber tests
 *
 */


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserList {
    List<UserResponse> users;
}
