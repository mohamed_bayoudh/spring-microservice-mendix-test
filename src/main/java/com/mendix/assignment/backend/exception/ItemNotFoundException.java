package com.mendix.assignment.backend.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ItemNotFoundException extends RuntimeException{

    private String message;

    public ItemNotFoundException(String message) {
        this.message = message;
    }



}