package com.mendix.assignment.backend.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CategoryNotFoundException extends RuntimeException{

    private String message;

    public CategoryNotFoundException(String message) {
        this.message = message;
    }




}