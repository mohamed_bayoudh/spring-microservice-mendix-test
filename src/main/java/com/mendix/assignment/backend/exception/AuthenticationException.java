package com.mendix.assignment.backend.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AuthenticationException extends RuntimeException{

    private String message;

    public AuthenticationException(String message) {
        this.message = message;
    }


}