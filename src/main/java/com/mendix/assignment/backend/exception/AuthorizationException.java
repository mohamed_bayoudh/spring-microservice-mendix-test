package com.mendix.assignment.backend.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AuthorizationException extends RuntimeException{

    private String message = "your user account is not authorized to get this information / perform this action, contact the admin to get admin role";

    public AuthorizationException(String message) {
        this.message = message;
    }


}