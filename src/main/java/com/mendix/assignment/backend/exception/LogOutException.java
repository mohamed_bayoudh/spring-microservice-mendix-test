package com.mendix.assignment.backend.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class LogOutException extends RuntimeException{

    private String message;

    public LogOutException(String message) {
        this.message = message;
    }


}