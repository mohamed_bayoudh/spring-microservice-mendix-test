package com.mendix.assignment.backend.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ItemAlreadyExistsException extends RuntimeException{

    private String message;

    public ItemAlreadyExistsException(String message) {
        this.message = message;
    }



}