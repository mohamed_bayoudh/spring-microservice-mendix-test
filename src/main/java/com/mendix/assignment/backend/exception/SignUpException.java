package com.mendix.assignment.backend.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SignUpException extends RuntimeException{

    private String message;

    public SignUpException(String message) {
        this.message = message;
    }


}