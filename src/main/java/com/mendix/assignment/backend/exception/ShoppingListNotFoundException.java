package com.mendix.assignment.backend.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ShoppingListNotFoundException extends RuntimeException{

    private String message;

    public ShoppingListNotFoundException(String message) {
        this.message = message;
    }




}