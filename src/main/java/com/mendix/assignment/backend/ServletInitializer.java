package com.mendix.assignment.backend;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * @author Mohamed Bayoudh
 *
 * Configuration class for the microservice starter, it executes the servlet initialisation,
 * based on the context collected by Spring
 *
 * @see org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
 *
 */

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(BackendApplication.class);
    }

}
