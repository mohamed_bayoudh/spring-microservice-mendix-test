package com.mendix.assignment.backend.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * @see org.springframework.security.web.access.AccessDeniedHandler
 *
 * This class is for the handling of the denied access and it tells Spring Security to redirect
 * the request to the following url: /auth/accessDenied
 */
@Configuration
public class CustomAccessDeniedHandler implements AccessDeniedHandler {


    @Override
    public void handle(
      HttpServletRequest request,
      HttpServletResponse response,
      AccessDeniedException exc) throws IOException, ServletException {
        response.sendRedirect(request.getContextPath() + "/auth/accessDenied");
    }
}