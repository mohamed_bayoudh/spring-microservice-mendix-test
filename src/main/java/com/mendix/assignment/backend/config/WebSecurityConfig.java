package com.mendix.assignment.backend.config;

import com.mendix.assignment.backend.utility.auth.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.savedrequest.NullRequestCache;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * @see org.springframework.security.config.annotation.SecurityConfigurer
 *
 * @see org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
 *
 * @see org.springframework.security.config.annotation.web.WebSecurityConfigurer
 *
 * WebSecurityConfig is the main configurator for Spring Security module
 * This class allows the microservice to configure the different security configurators/Filters and Beans
 *
 */


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
    prePostEnabled = true)
@EnableWebMvc
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
  @Autowired
  UserDetailsServiceImpl userDetailsService;

  @Override
  public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
    authenticationManagerBuilder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
  }



  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  @Bean
  public AccessDeniedHandler accessDeniedHandler(){
    return new CustomAccessDeniedHandler();
  }


  @Bean
  public HttpSessionEventPublisher httpSessionEventPublisher() {
    return new HttpSessionEventPublisher();
  }

  @Bean
  public BCryptPasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }


  /**
   *
   * @param http httpsecurity configurator
   * @throws Exception (different exceptions will be thrown)
   *
   * As it is commented in the code this bean configure all accesses
   *
   * In order to permit a request to get authenticated or get the access to the API, the request
   * headers and type should be compatible with those conditions.
   *
   *
   */

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    //Permit Post Requests
    http.csrf().disable().cors()
    //Permit Get-Requests for Swagger-Documentation
            .and().formLogin().disable()
            .authorizeRequests()
              .antMatchers("/swagger-ui/**", "/swagger-ui.html" ,"/webjars/**" ,"/v2/**" ,"/swagger-resources/**")
              .permitAll().and()

    //Permit the consumption of the API for authenticated users
    .authorizeRequests().antMatchers("/auth/**").permitAll()

            .and().authorizeRequests().anyRequest().authenticated().and()
    .logout().logoutUrl("/auth/signout").logoutSuccessUrl("/auth/logoutSuccess");
  }





}
