package com.mendix.assignment.backend.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 *
 * Simple class for the Swagger configuration
 * it redirects the access on the main endpoint directly to the swagger page, giving the user an overview over the whole API
 *
 */

@Configuration
public class SwaggerConfig implements WebMvcConfigurer {
    @Override
    public void addViewControllers(final ViewControllerRegistry registry) {
        registry.addRedirectViewController("/", "/swagger-ui/");
    }
}