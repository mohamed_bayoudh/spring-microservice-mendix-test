package com.mendix.assignment.backend;

import com.mendix.assignment.backend.dtos.ErrorDto;
import com.mendix.assignment.backend.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author Mohamed Bayoudh
 *
 * One of the most important handlers in the API, he catches all the defined exceptions and return a response entitiy
 * as a JSON object with the associated http code
 *
 *
 * @see  org.springframework.web.bind.annotation.ControllerAdvice;
 *
 */
@ControllerAdvice
public class MicroserviceExceptionHandler
  extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = UserNotFoundException.class)
    public ResponseEntity<Object> exception(UserNotFoundException e) {
        return new ResponseEntity<>(new ErrorDto(404,e.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = ItemNotFoundException.class)
    public ResponseEntity<Object> exception4(ItemNotFoundException e) {
        return new ResponseEntity<>(new ErrorDto(404,e.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = ItemAlreadyExistsException.class)
    public ResponseEntity<Object> exception4(ItemAlreadyExistsException e) {
        return new ResponseEntity<>(new ErrorDto(400,e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = ShoppingListNotFoundException.class)
    public ResponseEntity<Object> exception5(ShoppingListNotFoundException e) {
        return new ResponseEntity<>(new ErrorDto(404,e.getMessage()), HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(value = CategoryNotFoundException.class)
    public ResponseEntity<Object> exception5(CategoryNotFoundException e) {
        return new ResponseEntity<>(new ErrorDto(404,e.getMessage()), HttpStatus.NOT_FOUND);
    }


    // TODO: Exception not catched, need another method to catch 403 Response
    @ExceptionHandler(AccessDeniedException.class)
    @ResponseBody
    public String exception(AccessDeniedException e) {
        return "{\"status\":\"access denied for non authenticated users\"}";
    }

    @ExceptionHandler(value = AuthorizationException.class)
    public ResponseEntity<Object> exceptionAuthorization(AuthorizationException e) {
        return new ResponseEntity<>(new ErrorDto(403,e.getMessage()), HttpStatus.FORBIDDEN);
    }


    @ExceptionHandler(value = AuthenticationException.class)
    public ResponseEntity<Object> exceptionAuthentication(AuthenticationException e) {
        return new ResponseEntity<>(new ErrorDto(403,e.getMessage()), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = LogOutException.class)
    public ResponseEntity<Object> exceptionLogOut(LogOutException e) {
        return new ResponseEntity<>(new ErrorDto(200,e.getMessage()), HttpStatus.OK);
    }

    @ExceptionHandler(value = LogInException.class)
    public ResponseEntity<Object> exceptionLogin(LogInException e) {
        return new ResponseEntity<>(new ErrorDto(401,e.getMessage()), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = SignUpException.class)
    public ResponseEntity<Object> SignUpExcep(SignUpException e) {
        return new ResponseEntity<>(new ErrorDto(400,e.getMessage()), HttpStatus.BAD_REQUEST);
    }
}