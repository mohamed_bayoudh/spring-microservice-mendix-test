package com.mendix.assignment.backend.controllers;

import com.mendix.assignment.backend.mappers.ShoppingList.ShoppingListRequest;
import com.mendix.assignment.backend.mappers.ShoppingList.ShoppingListResponse;
import com.mendix.assignment.backend.services.ShoppingListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;




/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * This controller permit users to create/update or delete the shopping lists and to manage them
 *
 * @CrossOrigin permit the Cross-Origin requests to be caught by the spring security engine and get processed
 */

//@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/shoppingList")
public class ShoppingListController {

    @Autowired
    private ShoppingListService shoppingListService;

    @RequestMapping(value = "/findById/{shoppingListId}", method = RequestMethod.GET)
    ResponseEntity<ShoppingListResponse> findShoppingListById(@Valid @PathVariable("shoppingListId") long shoppingListId) {
        return new ResponseEntity<>(shoppingListService.findShoppingListById(shoppingListId),HttpStatus.OK);
    }

    @RequestMapping(value = "/findByActualUser", method = RequestMethod.GET)
    ResponseEntity<List<ShoppingListResponse>> findShoppingListsByActualUser() {
        return new ResponseEntity<>(shoppingListService.findShoppingListsByActualUser(),HttpStatus.OK);
    }

    @RequestMapping(value = "/findByTitle/{title}", method = RequestMethod.GET)
    ResponseEntity<ShoppingListResponse> findShoppingListByTitle(@Valid @PathVariable("title") String title) {
        return new ResponseEntity<>(shoppingListService.findByTitle(title),HttpStatus.OK);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    ResponseEntity<ShoppingListResponse> createShoppingList(@Valid @RequestBody ShoppingListRequest shoppingListRequest) {
        return new ResponseEntity<>(shoppingListService.createShoppingList(shoppingListRequest),HttpStatus.OK);
    }

    @RequestMapping(value = "/update/{shoppingListId}", method = RequestMethod.POST)
    ResponseEntity<ShoppingListResponse> updateShoppingList(@PathVariable("shoppingListId") long itemId ,@Valid @RequestBody ShoppingListRequest shoppingListRequest) {
        return new ResponseEntity<>(shoppingListService.updateShoppingList(shoppingListRequest,itemId),HttpStatus.OK);
    }


    @RequestMapping(value = "/addItem/{itemId}/{shoppingListId}", method = RequestMethod.POST)
    ResponseEntity<ShoppingListResponse> addItemToShoppingList(@PathVariable("itemId") long itemId, @PathVariable("shoppingListId") long shoppingListId) {
        return new ResponseEntity<>(shoppingListService.addItemToShoppingList(itemId,shoppingListId),HttpStatus.OK);
    }

    @RequestMapping(value = "/deleteItem/{itemId}/{shoppingListId}", method = RequestMethod.POST)
    ResponseEntity<ShoppingListResponse> deleteItemFromShoppingList(@PathVariable("itemId") long itemId,@PathVariable("shoppingListId")  long shoppingListId) {
        return new ResponseEntity<>(shoppingListService.deleteItemFromShoppingList(itemId,shoppingListId),HttpStatus.OK);
    }

    @RequestMapping(value = "/delete/{shoppingListId}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteShoppingList(@PathVariable("shoppingListId")  long shoppingListId) {
        shoppingListService.deleteShoppingList(shoppingListId);
        return ResponseEntity.ok("ShoppingList is deleted");
    }

    @RequestMapping(value = "/getShoppingLists", method = RequestMethod.GET)
    ResponseEntity<List<ShoppingListResponse>> findAllShoppingLists() {
        return new ResponseEntity<>(shoppingListService.findAllShoppingLists(), HttpStatus.OK);
    }


    @RequestMapping(value = "/updateTitle/{shoppingListId}", method = RequestMethod.POST)
    ResponseEntity<ShoppingListResponse> updateShoppingListTitle(@PathVariable("shoppingListId") long shoppingListId , @RequestBody String title) {
        return new ResponseEntity<>(shoppingListService.updateShoppingListTitle(title,shoppingListId),HttpStatus.OK);
    }

    @RequestMapping(value = "/updateDescription/{shoppingListId}", method = RequestMethod.POST)
    ResponseEntity<ShoppingListResponse> updateShoppingListDescription(@PathVariable("shoppingListId") long shoppingListId , @RequestBody String description) {
        return new ResponseEntity<>(shoppingListService.updateShoppingListDescription(description,shoppingListId),HttpStatus.OK);
    }


}
