package com.mendix.assignment.backend.controllers;

import com.mendix.assignment.backend.mappers.Item.ItemRequest;
import com.mendix.assignment.backend.mappers.Item.ItemResponse;
import com.mendix.assignment.backend.services.ItemService;
import com.mendix.assignment.backend.exception.AuthorizationException;
import com.mendix.assignment.backend.utility.auth.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * This controller permit users to get informations about the items which are in the database
 *
 * Also through this controller can admins delete and insert items from or into the database
 *
 * @CrossOrigin permit the Cross-Origin requests to be caught by the spring security engine and get processed
 */

//@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/items")
public class ItemController {

    @Autowired
    private ItemService itemService;

    @RequestMapping(value = "/getAllItems", method = RequestMethod.GET)
    ResponseEntity<List<ItemResponse>> getAllItems() {

        return new ResponseEntity<>(itemService.findAllItems(),HttpStatus.OK);
    }

    @RequestMapping(value = "/getItemsPerSet", method = RequestMethod.POST)
    ResponseEntity<List<ItemResponse>> getItemsPerSet(@Valid @RequestBody List<Long> itemIds) {
        return new ResponseEntity<>(itemService.getItemsPerSet(itemIds),HttpStatus.OK);
    }

    @RequestMapping(value = "/addItemInDb", method = RequestMethod.POST)
    ResponseEntity<ItemResponse> addItem(@Valid @RequestBody ItemRequest itemRequest) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        if(getRole(userDetails).equals("user")) throw new AuthorizationException();
        return new ResponseEntity<>(itemService.addItem(itemRequest),HttpStatus.OK);
    }

    @RequestMapping(value = "/deleteItemFromDb/{item}", method = RequestMethod.DELETE)
    ResponseEntity<String> deleteItem(@PathVariable("item") long itemId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        if(getRole(userDetails).equals("user")) throw new AuthorizationException();
        itemService.deleteItem(itemId);
        return ResponseEntity.ok("Item is deleted");
    }

    @RequestMapping(value = "/getItemsPerCategories", method = RequestMethod.GET)
    ResponseEntity<HashMap<String,List<Long>>> getItemsPerCategories() {
        return new ResponseEntity<>(itemService.getItemsPerCategories(),HttpStatus.OK);
    }


    @RequestMapping(value = "/getItemsPerCategory/{category}", method = RequestMethod.GET)
    ResponseEntity<List<Long>> getItemsPerCategory(@PathVariable("category") String category ) {
        return new ResponseEntity<>(itemService.getItemsPerCategory(category),HttpStatus.OK);
    }

    private String getRole(UserDetails userDetails){
        return userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList()).get(0);
    }

}
