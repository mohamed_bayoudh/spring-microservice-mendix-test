package com.mendix.assignment.backend.controllers;

import com.mendix.assignment.backend.mappers.User.UserResponse;
import com.mendix.assignment.backend.services.UserService;
import com.mendix.assignment.backend.exception.AuthorizationException;
import com.mendix.assignment.backend.utility.auth.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;




/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * This controller permit users to retrieve their account informations and admins to delete specific users
 * An admin can also grant admin rights to other users
 *
 * @CrossOrigin permit the Cross-Origin requests to be caught by the spring security engine and get processed
 */


//@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/userAsAdmin/{userId}", method = RequestMethod.POST)
    ResponseEntity<UserResponse> upgradeUser(@Valid @PathVariable("userId") Long userId) {
        if(getRole().equals("user")) throw new AuthorizationException();
        userService.upgradeUser(userId);
        return new ResponseEntity<>(userService.findUserById(userId), HttpStatus.OK);
    }

    @RequestMapping(value = "/getUsers", method = RequestMethod.GET)
    ResponseEntity<List<UserResponse>> getUsers() {
        if(getRole().equals("user")) throw new AuthorizationException();
        return new ResponseEntity<>(userService.findAllUsers(), HttpStatus.OK);
    }


    @RequestMapping(value = "/username/{username}", method = RequestMethod.GET)
    ResponseEntity<UserResponse> getUserByUsername(@PathVariable("username") String username) {
        if(getRole().equals("user")) throw new AuthorizationException();
        return new ResponseEntity<>(userService.findUserByUsername(username), HttpStatus.OK);
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
    ResponseEntity<UserResponse> getUserById(@PathVariable("id") Long id) {
        if(getRole().equals("user")) throw new AuthorizationException();
        return new ResponseEntity<>(userService.findUserById(id), HttpStatus.OK);
    }


    @RequestMapping(value = "/deleteById/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteUserByUsername(@PathVariable("id") Long id) {
        if(getRole().equals("user")) throw new AuthorizationException();
        userService.deleteUserById(id);
        return new ResponseEntity<>("User deleted successfully", HttpStatus.OK);
    }

    private String getRole(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        return userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList()).get(0);
    }


    @RequestMapping(value = "/getActualUser", method = RequestMethod.GET)
    ResponseEntity<UserResponse> getActualUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        System.out.println(SecurityContextHolder.getContext().getAuthentication().isAuthenticated());
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        UserResponse user = new UserResponse();
        user.setUsername(userDetails.getUsername());
        user.setEmail(userDetails.getEmail());
        user.setCreatedAt(userDetails.getCreatedAt());
        user.setUpdatedAt(userDetails.getUpdatedAt());
        user.setId(userDetails.getId());
        user.setRole(userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList()).get(0));
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

}




