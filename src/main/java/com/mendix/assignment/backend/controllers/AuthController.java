package com.mendix.assignment.backend.controllers;

import com.mendix.assignment.backend.dtos.authentication.LoginRequest;
import com.mendix.assignment.backend.dtos.authentication.SignupRequest;
import com.mendix.assignment.backend.dtos.UserDto;
import com.mendix.assignment.backend.exception.LogInException;
import com.mendix.assignment.backend.exception.SignUpException;
import com.mendix.assignment.backend.mappers.User.UserResponse;
import com.mendix.assignment.backend.repositories.UserRepository;
import com.mendix.assignment.backend.exception.AuthenticationException;
import com.mendix.assignment.backend.exception.LogOutException;
import com.mendix.assignment.backend.utility.auth.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;



/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * This controller permit users to authenticate themselves or to register if they don't have user accounts
 *
 * @CrossOrigin permit the Cross-Origin requests to be caught by the spring security engine and get processed
 */



//@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class AuthController {

  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  UserRepository userRepository;




  @RequestMapping(value = "/signin", method = RequestMethod.POST)
  public ResponseEntity<UserResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

    Authentication authentication = null;
    try{
      authentication = authenticationManager.authenticate(
              new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
    } catch (BadCredentialsException e){
      throw new LogInException("Error: Signing could not be done! Please check your credentials1");
    }

    SecurityContextHolder.getContext().setAuthentication(authentication);

    UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

    if (userDetails.getUsername().equals("")) {
      throw new LogInException("Error: Signing could not be done! Please check your credentials2");
    }

    List<String> roles = userDetails.getAuthorities().stream()
            .map(item -> item.getAuthority())
            .collect(Collectors.toList());

    RequestContextHolder.currentRequestAttributes().getSessionId();


    UserResponse user = new UserResponse();
    user.setId(userDetails.getId());
    user.setUsername(userDetails.getUsername());
    user.setEmail(userDetails.getEmail());
    user.setRole(roles.get(0));
    user.setUpdatedAt(userDetails.getUpdatedAt());
    user.setCreatedAt(userDetails.getCreatedAt());
    user.setJSessionId(RequestContextHolder.currentRequestAttributes().getSessionId());
    return new ResponseEntity<>(user, HttpStatus.OK);
  }

  @PostMapping("/signup")
  public ResponseEntity<UserResponse> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
    if (userRepository.existsByUsername(signUpRequest.getUsername()))
      throw new SignUpException("Error: Username is already taken!");


    if (userRepository.existsByEmail(signUpRequest.getEmail()))
      throw new SignUpException("Error: Email is already in use!");


    // Create new user's account
    UserDto user = new UserDto(signUpRequest.getUsername(),
               signUpRequest.getEmail(),
            signUpRequest.getPassword(),"user");

    userRepository.save(user);
    UserResponse userResp = new UserResponse();
    userResp.setId(user.getId());
    userResp.setUsername(user.getUsername());
    userResp.setEmail(user.getEmail());
    userResp.setRole(user.getRole());
    userResp.setUpdatedAt(user.getUpdatedAt());
    userResp.setCreatedAt(user.getCreatedAt());
    return new ResponseEntity<>(userResp, HttpStatus.OK);
  }

  @GetMapping("/logoutSuccess")
  public ResponseEntity<String> logoutUser() {
      throw new LogOutException("User logged out successfully!");
  }

  @GetMapping("/accessDenied")
  public ResponseEntity<String> accessDenied() {
    throw new AuthenticationException("Access Denied! Please provide valid login credentials!");
  }

}
