package com.mendix.assignment.backend.utility.auth;

import com.mendix.assignment.backend.dtos.UserDto;
import com.mendix.assignment.backend.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Mohamed Bayoudh
 *
 * This class is the responsible class on retrieving users from the microservice database,
 * it fetches the user repository and searchs the user by id and return it to the AuthenticationManger who
 * decide, if this user is authenticated or not
 *
 * @see org.springframework.security.core.userdetails.UserDetails
 *
 */

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
  @Autowired
  UserRepository userRepository;

  @Override
  @Transactional
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    UserDto user = userRepository.findByUsername(username);

    if (user == null)
      new UsernameNotFoundException("User Not Found with username: " + username);

    return UserDetailsImpl.build(user);
  }


}
