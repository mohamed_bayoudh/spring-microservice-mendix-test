package com.mendix.assignment.backend.utility.auditing;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.time.LocalDateTime;


/**
 *
 * @author Mohamed Bayoudh
 *
 * One of the important classes in the project, it is extended in all the entity classes.
 *
 * It allows the recording of the creation and updating time with the help of hibernate operations
 *
 * @see org.hibernate.annotations.CreationTimestamp;
 *
 * @see org.hibernate.annotations.UpdateTimestamp;
 *
 * @see java.io.Serializable
 *
 *
 */


@Getter
@Setter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class Auditable implements Serializable {

    @CreationTimestamp
    @Column(name="created_at", nullable = false, updatable = false)
    @CreatedDate
    private LocalDateTime createdAt;



    @UpdateTimestamp
    @LastModifiedDate
    @Column(name="updated_at",nullable = false)
    private LocalDateTime updatedAt;
}
