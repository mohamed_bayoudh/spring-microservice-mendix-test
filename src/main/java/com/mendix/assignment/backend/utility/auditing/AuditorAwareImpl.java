package com.mendix.assignment.backend.utility.auditing;

import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * @see org.springframework.security.web.access.AccessDeniedHandler
 *
 * Simple class for the AuditConfiguration, it defines and sets under which profile the auditing proces will be executed
 *
 */

public class AuditorAwareImpl implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.of("mendix");
    }

}
