package com.mendix.assignment.backend.services;

import com.mendix.assignment.backend.dtos.ItemDto;
import com.mendix.assignment.backend.mappers.Item.ItemMapper;
import com.mendix.assignment.backend.mappers.Item.ItemRequest;
import com.mendix.assignment.backend.mappers.Item.ItemResponse;
import com.mendix.assignment.backend.repositories.ItemRepository;
import com.mendix.assignment.backend.exception.CategoryNotFoundException;
import com.mendix.assignment.backend.exception.ItemAlreadyExistsException;
import com.mendix.assignment.backend.exception.ItemNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * A Service class for the management of requests of the items, it is classified as the intermediare
 * between the items controller and the items JPARepository
 *
 */

@Service
public class ItemService {
    @Autowired
    private ItemRepository itemRepository;

    private ItemMapper itemMapper = new ItemMapper();

    public ItemResponse findItemById(long id){
        if(itemRepository.existsById(id)){
            return itemMapper.mapDto(itemRepository.findById(id));
        }
         else
           throw new ItemNotFoundException("The searched Item with given id "+id+" not found");

    }

    public ItemResponse findItemByName(String name){
        ItemDto item = itemRepository.findByName(name);
        if(item != null){
            return itemMapper.mapDto(item);
        }
        else throw new ItemNotFoundException("The searched Item with given name "+name+" not found");

    }

    public List<ItemResponse> findAllItems(){
        return itemMapper.map(itemRepository.findAll());
    }

    public ItemResponse addItem(ItemRequest itemRequest) {
        ItemDto itemDto = itemRepository.findByName(itemRequest.getName());
        if(itemDto != null){
            throw new ItemAlreadyExistsException("Item "+itemRequest.getName()+" exists already in our Database");
        } else {
            ItemDto item = itemMapper.toDto(itemRequest);
            itemRepository.save(item);
            return itemMapper.mapDto(item);
        }

    }

    public void deleteItem(long id) {
        if(itemRepository.existsById(id)){
            itemRepository.deleteById(id);
        }
        else
            throw new ItemNotFoundException("The searched Item with given id "+id+" not found, Item can not be deleted");

    }

    public HashMap<String,List<Long>> getItemsPerCategories() {
        HashMap<String,List<Long>> result = new HashMap<>();
        List<ItemDto> list = itemRepository.findAll();
        List<String> itemCategories = list.stream().map(ItemDto::getCategories).flatMap(List::stream).distinct()
                .collect(Collectors.toList());
        for (String category:itemCategories
             ) {
            result.put(category,list.stream().filter(itemDto -> itemDto.getCategories().contains(category)).map(ItemDto::getId).collect(Collectors.toList()));
        }

        return result;
    }

    public List<Long> getItemsPerCategory(String category) {
        List<ItemDto> list = itemRepository.findByCategories(category);
        if(list.size() != 0){
            return list.stream().map(ItemDto::getId).collect(Collectors.toList());
        } else
            throw new CategoryNotFoundException("For the searched Category does not exist in any item in our Database");

    }

    public List<ItemResponse> getItemsPerSet(List<Long> itemIds) throws ItemNotFoundException {
        List<ItemResponse> list = new ArrayList<>();
        for (long id:itemIds
             ) {
            list.add(findItemById(id));
        }
        return list;
    }
}
