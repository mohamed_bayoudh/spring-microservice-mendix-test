package com.mendix.assignment.backend.services;

import com.mendix.assignment.backend.dtos.ItemDto;
import com.mendix.assignment.backend.dtos.ShoppingListDto;
import com.mendix.assignment.backend.mappers.ShoppingList.ShoppingListMapper;
import com.mendix.assignment.backend.mappers.ShoppingList.ShoppingListRequest;
import com.mendix.assignment.backend.mappers.ShoppingList.ShoppingListResponse;
import com.mendix.assignment.backend.repositories.ItemRepository;
import com.mendix.assignment.backend.repositories.ShoppingListRepository;
import com.mendix.assignment.backend.exception.*;
import com.mendix.assignment.backend.utility.auth.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * A Service class for the management of requests of the shopping lists, it is classified as the intermediare
 * between the shopping list controller and the shopping list JPARepository
 *
 */


@Service
public class ShoppingListService {
    @Autowired
    private ShoppingListRepository shoppingListRepository;

    @Autowired
    private ItemRepository itemRepository;

    private ShoppingListMapper shoppingListMapper = new ShoppingListMapper();


    public ShoppingListResponse findShoppingListById(long shoppingListId){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        if(shoppingListRepository.existsById(shoppingListId)) {
            ShoppingListDto shoppingListDto = shoppingListRepository.findById(shoppingListId);
            if ((shoppingListDto.getUserID() != userDetails.getId()) && (getRole(userDetails).equals("user")))
                throw new AuthorizationException();
            return shoppingListMapper.mapDto(shoppingListDto); }
        else
            throw new ShoppingListNotFoundException("Shopping List not Found");
    }


    public ShoppingListResponse findByTitle(String title){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        ShoppingListDto shoppingListDto = shoppingListRepository.findByTitle(title);
        if(shoppingListDto != null){
            if((shoppingListDto.getUserID() != userDetails.getId())&&(getRole(userDetails).equals("user"))) throw new AuthorizationException();
            return shoppingListMapper.mapDto(shoppingListDto); }
        else
            throw new ShoppingListNotFoundException("Shopping List not Found");
    }


    public List<ShoppingListResponse> findAllShoppingLists(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        if(getRole(userDetails).equals("user"))
            return shoppingListMapper.map(shoppingListRepository.findAll().stream()
                .filter(shoppingListDto -> shoppingListDto.getUserID() == userDetails.getId())
                .collect(Collectors.toList()));
        else
            return shoppingListMapper.map(shoppingListRepository.findAll());
    }


    public ShoppingListResponse createShoppingList(ShoppingListRequest shoppingListRequest) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        ShoppingListDto shoppingListDto = shoppingListMapper.toDto(shoppingListRequest);
        shoppingListDto.setUserID(userDetails.getId());
        if(itemRepository.findAll().stream().map(ItemDto::getId).collect(Collectors.toList()).containsAll(shoppingListDto.getItemList())){
            shoppingListRepository.save(shoppingListDto);
            return shoppingListMapper.mapDto(shoppingListDto);
        } else
            throw new ItemNotFoundException("Item ID included in the body not Found, Shopping List can not be created");
    }

    public ShoppingListResponse addItemToShoppingList(long itemId, long shoppingListId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        if(shoppingListRepository.existsById(shoppingListId)){
            ShoppingListDto shoppingListDto = shoppingListRepository.findById(shoppingListId);
            if((shoppingListDto.getUserID() != userDetails.getId())&&(getRole(userDetails).equals("user"))) throw new AuthorizationException();
            if(itemRepository.existsById(itemId)){
                if(!shoppingListDto.getItemList().contains(itemId)){
                    shoppingListDto.getItemList().add(itemId);
                    shoppingListDto.setItemList(shoppingListDto.getItemList().stream().collect(Collectors.toList()));
                    shoppingListRepository.save(shoppingListDto);
                    return shoppingListMapper.mapDto(shoppingListDto);
                }
                else
                    throw new ItemAlreadyExistsException("Item ID already included in the Shopping List");
            } else
                throw new ItemNotFoundException("Item ID included in the body not Found, Shopping List can not be updated");
        } else
            throw new ShoppingListNotFoundException("Shopping List not Found, Item can not be added");

    }

    public ShoppingListResponse deleteItemFromShoppingList(long itemId, long shoppingListId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        if(shoppingListRepository.existsById(shoppingListId)){
            ShoppingListDto shoppingListDto = shoppingListRepository.findById(shoppingListId);
            if((shoppingListDto.getUserID() != userDetails.getId())&&(getRole(userDetails).equals("user"))) throw new AuthorizationException();
            if(itemRepository.existsById(itemId)){
                if(shoppingListDto.getItemList().contains(itemId)){
                    shoppingListDto.getItemList().remove(itemId);
                    shoppingListDto.setItemList(shoppingListDto.getItemList().stream().collect(Collectors.toList()));
                    shoppingListRepository.save(shoppingListDto);
                    return shoppingListMapper.mapDto(shoppingListDto);
                } else
                    throw new ItemNotFoundException("Item ID not included in the shopping list, item List for this list can not be updated");

            } else
                throw new ItemNotFoundException("Item ID included in the body not Found, Shopping List can not be updated");
        } else
            throw new ShoppingListNotFoundException("Shopping List not Found, Item can not be added");

    }

    public void deleteShoppingList(long shoppingListId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        if(shoppingListRepository.existsById(shoppingListId)){
            ShoppingListDto shoppingListDto = shoppingListRepository.findById(shoppingListId);
            if((shoppingListDto.getUserID() != userDetails.getId())&&(getRole(userDetails).equals("user"))) throw new AuthorizationException();
            shoppingListRepository.deleteById(shoppingListId);
        } else
            throw new ShoppingListNotFoundException("Shopping List not Found to be deleted");
    }

    public ShoppingListResponse updateShoppingList(ShoppingListRequest shoppingListRequestNew, long shoppingListId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        if(shoppingListRepository.existsById(shoppingListId)){
            ShoppingListDto shoppingListDto = shoppingListMapper.toDto(shoppingListRequestNew);
            if(itemRepository.findAll().stream().map(ItemDto::getId).collect(Collectors.toList()).containsAll(shoppingListDto.getItemList())){
                ShoppingListDto shoppingListDtoOld = shoppingListRepository.findById(shoppingListId);
                if((shoppingListDto.getUserID() != userDetails.getId())&&(getRole(userDetails).equals("user"))) throw new AuthorizationException();
                shoppingListDto.setId(shoppingListDtoOld.getId());
                shoppingListDto.setCreatedAt(shoppingListDtoOld.getCreatedAt());
                shoppingListRepository.save(shoppingListDto);
                return shoppingListMapper.mapDto(shoppingListRepository.findById(shoppingListId));
            } else
                throw new ItemNotFoundException("Item ID included in the body not Found, Shopping List can not be updated");

        } else
            throw new ShoppingListNotFoundException("Shopping List with id "+shoppingListId+" not Found!");

     }

    public ShoppingListResponse updateShoppingListTitle(String title, long shoppingListId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        if(shoppingListRepository.existsById(shoppingListId)){
            ShoppingListDto shoppingListDto = shoppingListRepository.findById(shoppingListId);
            if((shoppingListDto.getUserID() != userDetails.getId())&&(getRole(userDetails).equals("user"))) throw new AuthorizationException();
            shoppingListDto.setTitle(title);
            shoppingListRepository.save(shoppingListDto);
            return shoppingListMapper.mapDto(shoppingListRepository.findById(shoppingListId));
        } else
            throw new ShoppingListNotFoundException("Shopping List not Found");
    }

    public ShoppingListResponse updateShoppingListDescription(String description, long shoppingListId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        if(shoppingListRepository.existsById(shoppingListId)){
            ShoppingListDto shoppingListDto = shoppingListRepository.findById(shoppingListId);
            if((shoppingListDto.getUserID() != userDetails.getId())&&(getRole(userDetails).equals("user"))) throw new AuthorizationException();
            shoppingListDto.setDescription(description);
            shoppingListRepository.save(shoppingListDto);
            return shoppingListMapper.mapDto(shoppingListRepository.findById(shoppingListId));
        } else
            throw new ShoppingListNotFoundException("Shopping List not Found");
    }

    private String getRole(UserDetails userDetails){
        return userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList()).get(0);
    }

    public List<ShoppingListResponse> findShoppingListsByActualUser() {
        List<ShoppingListResponse> list = findAllShoppingLists();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        return list.stream().filter(shoppingListDto -> shoppingListDto.getUserID() == userDetails.getId()).collect(Collectors.toList());
    }
}
