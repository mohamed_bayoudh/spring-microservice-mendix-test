package com.mendix.assignment.backend.services;

import com.mendix.assignment.backend.dtos.UserDto;
import com.mendix.assignment.backend.mappers.User.UserMapper;
import com.mendix.assignment.backend.mappers.User.UserResponse;
import com.mendix.assignment.backend.repositories.UserRepository;
import com.mendix.assignment.backend.exception.UserNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * A Service class for the management of requests of the users, it is classified as the intermediare
 * between the users controller and the users JPARepository
 *
 */
@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    private UserMapper userMapper = new UserMapper();

    public UserResponse findUserById(long id){
        if(userRepository.existsById(id)) {
            return userMapper.mapDto(userRepository.findById(id));
        }
        else throw new UserNotFoundException("User with ID "+ id + " not found");

    }

    public UserResponse findUserByUsername(String username){
        UserDto user = userRepository.findByUsername(username);
        if(user != null) {
            return userMapper.mapDto(user);
        }
        else throw new UserNotFoundException("User with username "+ username + " not found");
    }

    public List<UserResponse> findAllUsers(){
        return userMapper.map(userRepository.findAll());
    }


    public void deleteUserById(Long id) {
        if(userRepository.existsById(id)) userRepository.deleteById(id);
        else throw new UserNotFoundException("User with ID "+ id + " not found, User can not be deleted");
    }

    public void upgradeUser(long id) {
        if(userRepository.existsById(id)) {
           UserDto oldUser = userRepository.findById(id);
           oldUser.setRole("admin");
           userRepository.save(oldUser);
        }
        else throw new UserNotFoundException("User with ID "+ id + " not found, User can not be upgraded");


    }
}
