package com.mendix.assignment.backend.repositories;

import com.mendix.assignment.backend.dtos.ItemDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * An interface for the JPA Repository of the item entity, where the most used and important beans are defined
 *
 */

@Repository
public interface ItemRepository extends JpaRepository<ItemDto,Long> {

    ItemDto findById(long id);

    ItemDto findByName(String name);

    List<ItemDto> findByCategories(String category);

    List<ItemDto> findAll();

}
