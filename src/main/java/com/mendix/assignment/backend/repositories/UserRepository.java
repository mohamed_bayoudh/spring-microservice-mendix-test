package com.mendix.assignment.backend.repositories;

import com.mendix.assignment.backend.dtos.UserDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * An interface for the JPA Repository of the user entity, where the most used and important beans are defined
 *
 */

@Repository
public interface UserRepository extends JpaRepository<UserDto,Long> {

    UserDto findByUsername(String username);

    UserDto findById(long id);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    void deleteById(Long id);

    List<UserDto> findAll();


}
