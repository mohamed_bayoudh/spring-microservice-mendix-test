package com.mendix.assignment.backend.repositories;

import com.mendix.assignment.backend.dtos.ShoppingListDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * An interface for the JPA Repository of the shopping list entity, where the most used and important beans are defined
 *
 */


@Repository
public interface ShoppingListRepository extends JpaRepository<ShoppingListDto,Long> {
    ShoppingListDto findByTitle(String title);

    ShoppingListDto findById(long id);

    List<ShoppingListDto> findAll();
}
