package com.mendix.assignment.backend.dtos;

import com.mendix.assignment.backend.utility.auditing.Auditable;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * A DTO-Class for the items of a shopping list, it inherits the Auditable class which contains two attributes: createdAt and updated at
 *
 * @see com.mendix.assignment.backend.utility.auditing.Auditable
 *
 *
 */


@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
@Table(name = "item")
public class ItemDto extends Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    @NotNull
    private String name;

    @NotEmpty
    @NotNull
    private String description;

    @NotNull
    private double price;


    @ElementCollection
    @CollectionTable(name="categories")
    private List<String> categories = new ArrayList<>();


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemDto itemDto = (ItemDto) o;
        return Objects.equals(id, itemDto.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id,name,description,price);
    }
}
