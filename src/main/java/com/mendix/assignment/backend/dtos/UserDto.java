package com.mendix.assignment.backend.dtos;


import com.mendix.assignment.backend.utility.auditing.Auditable;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;


/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * A DTO-Class for the user entity, it inherits the Auditable class which contains two attributes: createdAt and updated at
 *
 *
 * @see com.mendix.assignment.backend.utility.auditing.Auditable
 *
 *
 */



@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
@Table(name = "user")
public class UserDto extends Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    @NotNull
    private String username;


    @NotEmpty
    @NotNull
    private String password;

    @NotEmpty
    @NotNull
    private String email;


    @NotEmpty
    @NotNull
    private String role;

    public UserDto(String username, String email, String password, String role) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        this.username = username;
        this.email = email;
        this.password = passwordEncoder.encode(password);
        this.role = role;
    }

    public UserDto() {
    }

    /**
     *
     * @param password
     *
     * This method is very important for the authentication because without the encryption of the password before
     * its insertion into the database, the authentication of the user will always fails
     *
     */
    public void setPassword(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        this.password = passwordEncoder.encode(password);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserDto)) return false;
        UserDto userDto = (UserDto) o;
        return Objects.equals(id, userDto.id) && Objects.equals(username, userDto.username) && Objects.equals(email, userDto.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id,username,email,role);
    }

    @Override
    public String toString() {
        return password+email+role;
    }
}