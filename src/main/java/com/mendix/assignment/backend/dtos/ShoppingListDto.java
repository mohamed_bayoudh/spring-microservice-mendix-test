package com.mendix.assignment.backend.dtos;


import com.mendix.assignment.backend.utility.auditing.Auditable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * A DTO-Class for the shopping list entity, it inherits the Auditable class which contains two attributes: createdAt and updated at
 *
 * @see com.mendix.assignment.backend.utility.auditing.Auditable
 *
 *
 */



@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Table(name = "shoppinlist")
public class ShoppingListDto extends Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @NotEmpty
    private String title;

    @NotNull
    private long userID;


    @NotNull
    @NotEmpty
    private String description;


    @NotNull
    @NotEmpty
    private String category;


    @NotNull
    @NotEmpty
    @ElementCollection
    @CollectionTable(name="listOfItems")
    private List<Long> itemList = new ArrayList<>();


    public ShoppingListDto(String title, long userID, String description, String category, List<Long> itemList) {
        this.title = title;
        this.userID = userID;
        this.description = description;
        this.category = category;
        this.itemList = itemList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ShoppingListDto)) return false;
        ShoppingListDto that = (ShoppingListDto) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id,title,userID,category);
    }

}
