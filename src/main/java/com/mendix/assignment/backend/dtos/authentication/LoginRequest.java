package com.mendix.assignment.backend.dtos.authentication;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;


/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * Simple class for the mapping of the login credentials and to get them wrapped
 *
 */


@Getter
@AllArgsConstructor
@NoArgsConstructor
@Setter
public class LoginRequest {

	@NotBlank
  	private String username;

	@NotBlank
	private String password;




}
