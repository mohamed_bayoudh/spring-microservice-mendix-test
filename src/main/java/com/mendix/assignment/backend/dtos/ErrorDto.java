package com.mendix.assignment.backend.dtos;

import lombok.*;

/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * Simple class for httpCodes and their exceptions, through this class the consumer of the API get an error message within JSON message and not as plain text
 *
 */


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ErrorDto {
    private Integer httpCode;
    private String message;
}
