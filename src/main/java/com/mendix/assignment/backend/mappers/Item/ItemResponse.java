package com.mendix.assignment.backend.mappers.Item;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * Simple class for the mapping of the item response and get them wrapped
 *
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ItemResponse {
    private Long id;

    private String name;

    private String description;

    private double price;


    private List<String> categories = new ArrayList<>();

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

}
