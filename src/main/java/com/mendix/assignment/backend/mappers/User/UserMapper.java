package com.mendix.assignment.backend.mappers.User;

import com.mendix.assignment.backend.dtos.UserDto;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * A mapper class for the user entity, which converts an item to the item response and an item request to an item entity
 *
 * @see com.mendix.assignment.backend.dtos.UserDto
 *
 * @see com.mendix.assignment.backend.mappers.User.UserResponse
 *
 * @see com.mendix.assignment.backend.mappers.User.UserRequest
 *
 */


public class UserMapper {
    public List<UserResponse> map(List<UserDto> list){
        return list.stream().map(this::mapDto).collect(Collectors.toList());
    }
    public UserResponse mapDto(UserDto user) {
        UserResponse userDto = new UserResponse();
        userDto.setId(user.getId());
        userDto.setUsername(user.getUsername());
        userDto.setEmail(user.getEmail());
        userDto.setRole(user.getRole());
        userDto.setCreatedAt(user.getCreatedAt());
        userDto.setUpdatedAt(user.getUpdatedAt());
        return userDto;
    }




}
