package com.mendix.assignment.backend.mappers.Item;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * Simple class for the mapping of the item request and get them wrapped
 *
 */


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ItemRequest {

    @NotBlank
    private String name;

    @NotBlank
    private String description;


    private double price;

    private List<String> categories = new ArrayList<>();
}
