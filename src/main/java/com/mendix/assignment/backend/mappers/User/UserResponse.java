package com.mendix.assignment.backend.mappers.User;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * Simple class for the mapping of the user response and get them wrapped
 *
 */


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {

    private Long id;

    private String username;

    private String email;

    private String role;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private String jSessionId;

}
