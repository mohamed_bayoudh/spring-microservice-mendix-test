package com.mendix.assignment.backend.mappers.ShoppingList;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;



/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * Simple class for the mapping of the shopping list response and get them wrapped
 *
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ShoppingListResponse {

    private Long id;

    private String title;

    private long userID;

    private List<Long> itemList = new ArrayList<>();

    private String category;

    private String description;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

}
