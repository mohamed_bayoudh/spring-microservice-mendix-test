package com.mendix.assignment.backend.mappers.ShoppingList;

import com.mendix.assignment.backend.dtos.ShoppingListDto;

import java.util.List;
import java.util.stream.Collectors;


/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * A mapper class for the shopping list entity, which converts an item to the item response and an item request to an item entity
 *
 * @see com.mendix.assignment.backend.dtos.ShoppingListDto
 *
 * @see com.mendix.assignment.backend.mappers.ShoppingList.ShoppingListRequest
 *
 * @see com.mendix.assignment.backend.mappers.ShoppingList.ShoppingListResponse
 *
 */


public class ShoppingListMapper {
    public List<ShoppingListResponse> map(List<ShoppingListDto> list){
        return list.stream().map(this::mapDto).collect(Collectors.toList());
    }
    public ShoppingListResponse mapDto(ShoppingListDto shoppingListDto) {
        ShoppingListResponse shoppingListResponse = new ShoppingListResponse();
        shoppingListResponse.setId(shoppingListDto.getId());
        shoppingListResponse.setTitle(shoppingListDto.getTitle());
        shoppingListResponse.setItemList(shoppingListDto.getItemList());
        shoppingListResponse.setCreatedAt(shoppingListDto.getCreatedAt());
        shoppingListResponse.setUpdatedAt(shoppingListDto.getUpdatedAt());
        shoppingListResponse.setCategory(shoppingListDto.getCategory());
        shoppingListResponse.setUserID(shoppingListDto.getUserID());
        shoppingListResponse.setDescription(shoppingListDto.getDescription());

        return shoppingListResponse;
    }

    public ShoppingListDto toDto(ShoppingListRequest shoppingListRequest) {
        ShoppingListDto shoppingListDto = new ShoppingListDto();
        shoppingListDto.setItemList(shoppingListRequest.getItemList());
        shoppingListDto.setTitle(shoppingListRequest.getTitle());
        shoppingListDto.setCategory(shoppingListRequest.getCategory());
        shoppingListDto.setDescription(shoppingListRequest.getDescription());
        return shoppingListDto;
    }
}
