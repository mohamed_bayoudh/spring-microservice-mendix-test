package com.mendix.assignment.backend.mappers.Item;

import com.mendix.assignment.backend.dtos.ItemDto;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * A mapper class for the item entity, which converts an item to the item response and an item request to an item entity
 *
 * @see com.mendix.assignment.backend.dtos.ItemDto
 *
 * @see com.mendix.assignment.backend.mappers.Item.ItemResponse
 *
 * @see com.mendix.assignment.backend.mappers.Item.ItemRequest
 *
 */

public class ItemMapper {


    public List<ItemResponse> map(List<ItemDto> list){
        return list.stream().map(this::mapDto).collect(Collectors.toList());
    }

    public ItemResponse mapDto(ItemDto itemDto) {
        ItemResponse item = new ItemResponse();
        item.setId(itemDto.getId());
        item.setCategories(itemDto.getCategories());
        item.setDescription(itemDto.getDescription());
        item.setName(itemDto.getName());
        item.setPrice(itemDto.getPrice());
        item.setCreatedAt(itemDto.getCreatedAt());
        item.setUpdatedAt(itemDto.getUpdatedAt());
        return item;
    }

    public ItemDto toDto(ItemRequest itemRequest) {
        ItemDto item = new ItemDto();
        item.setCategories(itemRequest.getCategories());
        item.setDescription(itemRequest.getDescription());
        item.setPrice(itemRequest.getPrice());
        item.setName(itemRequest.getName());
        return item;
    }


}
