package com.mendix.assignment.backend.mappers.ShoppingList;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Mohamed Bayoudh
 *
 * @version 1.0.0
 *
 * Simple class for the mapping of the shopping list request and get them wrapped
 *
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ShoppingListRequest {

    @NotBlank
    private String title;

    private String description;

    private String category;

    private List<Long> itemList = new ArrayList<>();


}
