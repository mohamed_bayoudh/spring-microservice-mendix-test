Feature: Signing up in the microservice API

  Scenario: Successful signing up
    Given I have valid register credentials
    When I sign up with valid credentials
    Then new user will be created with the given credentials

  Scenario: Signing up failed (Username taken)
    Given I try to sign up using already taken username
    When I sign up with invalid credentials
    Then new user not created

  Scenario: Signing up failed (Email taken)
    Given I try to sign up using already taken email
    When I sign up with invalid credentials
    Then new user not created