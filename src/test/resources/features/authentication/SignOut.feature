Feature: Signing out from the microservice API

  Scenario: Successful signing out
    Given I am signed in
    When I sign out
    Then I get Logout message
