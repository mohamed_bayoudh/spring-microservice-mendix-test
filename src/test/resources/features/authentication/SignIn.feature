Feature: Signing in to the microservice API

  Scenario: Successful signing in
    Given I have valid credentials
    When I sign in
    Then my user information are returned

  Scenario: Signing in failed
    Given I have non-valid credentials
    When I sign in with non-valid credentials
    Then my user information are not returned and error message is returned with the information that access denied because of non-valid credentials