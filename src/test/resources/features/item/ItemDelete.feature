Feature: Delete of an item

  Scenario: Successful delete of an item
    Given I have the possibility to delete an item from the DB by id
    When I delete the item by id
    Then item is deleted

  Scenario: Delete of item failed
    Given I try to delete an item by id which is not in the DB
    When I delete the item by id
    Then Error message is returned with the information that item not found in the DB