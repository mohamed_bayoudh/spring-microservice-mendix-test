Feature: Insertion of an item in the Db

  Scenario: Successful insertion of an item
    Given I have an item to insert into the DB (User: Admin)
    When I insert the item
    Then item is returned with its internal informations

  Scenario: Creation of a shopping list failed
    Given I have an already existing-item and I want to insert it into the DB (User: Admin)
    When I insert the item
    Then Error message is returned with the information that item already in the DB
