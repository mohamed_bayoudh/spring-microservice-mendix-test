Feature: Retrieving Item(s)

  Scenario: Successful retrieving of items by category
    Given I have a category of existing items
    When I try to get the items by category
    Then items are retrieved

  Scenario: Retrieving failed of of items by category
    Given I have a category of not-existing items
    When I try to get the items by category
    Then Error message is returned with the information that category not found in the DB



  Scenario: Successful retrieving of items per set
    Given List of item ids which are all inserted in the Db
    When I try to get the items of the given Ids
    Then Items with specified Ids are successfully retrieved

  Scenario: Retrieving of items per set failed
    Given List of items ids which are not all inserted in the Db
    When I try to get the items of the given Ids
    Then Items are not retrieved and Error message is returned with the information that item not found in the DB


  Scenario: Successful retrieving of item sets clustered by category
    When I try to get different item sets clustered by category
    Then items are returned and clustered by category



  Scenario: Successful retrieving of all items
    When I try to get all items
    Then all items are retrieved
