Feature: Delete of a shopping list

  Scenario: Successful delete of a shopping list
    Given I have the possibility to delete a shopping list by id
    When I delete a shopping list by id
    Then the shopping list is deleted

  Scenario: Delete of a shopping list failed
    Given I try to delete a shopping list by id which is not created yet
    When I delete a shopping list by id
    Then Error message is returned with the information that shopping list not found in the DB





