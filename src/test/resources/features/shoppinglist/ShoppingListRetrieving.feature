Feature: Retrieving a shopping list

  Scenario: Successful retrieving of a shopping list by id
    Given I have an id of an existing shopping list
    When I try to get the shopping list by id
    Then shopping list retrieved


  Scenario: Retrieving failed of a shopping list by id
    Given I have an id of non existing shopping list
    When I try to get the shopping list by id
    Then Error message is returned with the information that shopping list not found in the DB

  Scenario: Successful retrieving of a shopping list for the actual user
    Given The actual user created 2 shopping lists
    When I try to get the shopping list by actual user
    Then I get two shopping lists


  Scenario: Successful retrieving of all shopping lists (for admin)
    When I try to get all shopping lists as admin
    Then all shopping lists are retrieved



  Scenario: Successful retrieving of a shopping list by title
    Given I have a title of an existing shopping list
    When I try to get the shopping list by title
    Then shopping list retrieved


  Scenario: Retrieving of a shopping list by non-existing title failed
    Given I have a title of non existing shopping list
    When I try to get the shopping list by title
    Then Error message is returned with the information that shopping list not found in the DB






