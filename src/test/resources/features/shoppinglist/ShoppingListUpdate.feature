Feature: Updating a shopping list

  Scenario: Successful Replacing of a shopping list by Id
    Given I have a shopping list and I want to save it under a specific Id which is existing on the DB
    When I update shopping list under the Id
    Then The shopping list updated

  Scenario: Replacing of a shopping list by Id failed
    Given I have a shopping list and I want to save it under a specific Id which is not existing on the DB
    When I update shopping list under the specific Id
    Then Error message is returned with the information that shopping list not found in the DB


  Scenario: Successful updating of the title of a shopping list
    Given I have a shopping list id and I want to update its title
    When I retrieve shopping list by id and I update its title
    Then The shopping list title is updated

  Scenario: Updating of the title of a shopping list failed
    Given I have a shopping list id of other user and I want to update its title
    When I retrieve shopping list by id and I update its title
    Then Error message is returned with the information that shopping list not found in the DB


  Scenario: Successful updating of the description of a shopping list
    Given I have a shopping list id and I want to update its description
    When I retrieve shopping list by id and I update its description
    Then The shopping list description is updated

  Scenario: Updating of the description of a shopping list failed
    Given I have a shopping list id of other user and I want to update its description
    When I retrieve shopping list by id and I update its description
    Then Error message is returned with the information that shopping list not found in the DB

  Scenario: Successful adding item to a shopping list
    Given I have a shopping list id and an item Id
    When I retrieve shopping list by Id and I add the item to the list
    Then item is added to the list

  Scenario: Adding item to a shopping list failed
    Given I have a shopping list id and an item Id which is not in the Db
    When I retrieve shopping list by Id and I add the item to the list
    Then Error message is returned with the information that item not found in the DB

  Scenario: Adding item to a shopping list failed
    Given I have a shopping list id of other user and an item Id
    When I retrieve shopping list by Id and I add the item to the list
    Then Error message is returned with the information that he does not the right to update shopping list of other users

  Scenario: Successful deleting item from a shopping list
    Given I have a shopping list id and an item Id
    When I retrieve shopping list by Id and I delete the item from its list
    Then item deleted from the list

  Scenario: Deleting item from a shopping list failed (ItemNotFound)
    Given I have a shopping list id and an item Id which is not in the list
    When I retrieve shopping list by Id and I delete the item from the list
    Then Error message is returned with the information that item not found in the list

  Scenario: Deleting item from a shopping list failed (ShoppingListNotFound)
    Given I have a shopping list id of other user and an item Id
    When I retrieve shopping list by Id and I delete the item from the list
    Then Error message is returned with the information that he does not the right to update shopping list of other users