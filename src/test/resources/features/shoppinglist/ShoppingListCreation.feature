Feature: Creating a shopping list

  Scenario: Successful creation of a shopping list
    Given I have the possibility to create a shopping list
    When I create a shopping list with the title "Shopping List"
    Then the shopping list with the title "Shopping List" is created

  Scenario: Creation of a shopping list failed
    Given I have the possibility to create a shopping list
    When I create a shopping list with the title ""
    Then no list is created





