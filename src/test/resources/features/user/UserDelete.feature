Feature: Delete of an user

  Scenario: Successful delete of an user
    Given I have the possibility as admin to delete an user from the DB by id
    When I delete the user by id
    Then user is deleted

  Scenario: Delete of user failed (user not found)
    Given I try to delete an user by id which is not in the DB
    When I delete the user by id
    Then Error message is returned with the information that user not found in the DB

  Scenario: Delete of user failed (not Admin)
    Given As a user i want to delete another user from the DB by id
    When I delete the user by id
    Then Error message is returned with the information that action forbidden