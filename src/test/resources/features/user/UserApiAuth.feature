Feature: Accessing the user API just for admins

  Scenario: Successful retrieving of all users as amin
    Given I am signed in as an Admin
    When I try to get all users as admin
    Then all users are retrieved

  Scenario: Retrieving of all users as user failed
    Given I am signed in as a user
    When I try to get all users as user
    Then Error message is returned with the information that action forbidden