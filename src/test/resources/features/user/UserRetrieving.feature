Feature: Retrieving user(s)

  Scenario: Successful retrieving of a user by id
    Given I have an id of an existing user in the DB
    When I try to get the user by id
    Then user successfully retrieved


  Scenario: Retrieving failed of a user by id
    Given I have an id of an non-existing user in the DB
    When I try to get the user by id
    Then Error message is returned with the information that the user not found in the DB


  Scenario: Successful retrieving of a user by username
    Given I have a username of an existing user in the DB
    When I try to get the user by username
    Then user successfully retrieved


  Scenario: Retrieving failed of a user by username
    Given I have a username of an non-existing user in the DB
    When I try to get the user by username
    Then Error message is returned with the information that the user not found in the DB


  Scenario: Successful retrieving of all users
    When I try to get all users
    Then all users are retrieved
