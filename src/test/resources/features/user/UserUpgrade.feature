Feature: Granting a user admin role

  Scenario: Successful granting of admin right
    Given As admin I want to grant admin rights to another user by id
    When I grant admin right to user by id
    Then user with id is admin

  Scenario: Granting of admin right failed
    Given As admin I want to grant admin rights to another user by non-existing id in the Db
    When I grant admin right to user by id
    Then Error message is returned with the information that user not found in the DB