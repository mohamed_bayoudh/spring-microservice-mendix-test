package com.mendix.assignment.backend.repositories;

import com.mendix.assignment.backend.dtos.UserDto;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.List;
import java.util.stream.Collectors;


public class UserRepositoryTest extends RepositoryTestLoader {

    /*
    *  Repositories are overrided from JPARepository and they are already tested
    *  This Test class ensure that the data loading of all users in users.json file is done successfully and the constraints of the application are not violated
    */

    @Test
    public void FindAllUsers() {
        List<UserDto> users = userRepository.findAll();
        Assertions.assertEquals(4, users.size());
    }

    @Test
    public void getFirstUser() {
        UserDto user = userRepository.findById(1);
        Assertions.assertEquals("LaraBeerenburg",user.getUsername());
    }

    /*
     * The number of admins in the first Dataset of users must be 1, Admin rights could be granted through the UserController /upgradeUser/{userId}
     */
    @Test
    public void FindAdminsNumber() {
        int adminsNumber = userRepository.findAll().stream().filter(userDto -> userDto.getRole().equals("admin")).collect(Collectors.toList()).size();
        Assertions.assertEquals(1, adminsNumber);
    }


}
