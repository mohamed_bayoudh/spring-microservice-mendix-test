package com.mendix.assignment.backend.repositories;



import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;


/***
 * base-class for running repositories tests
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
class RepositoryTestLoader {

    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected ItemRepository itemRepository;


    @BeforeEach()
    protected void setUp() {
        this.userRepository.deleteAll();
        this.itemRepository.deleteAll();
    }



}
