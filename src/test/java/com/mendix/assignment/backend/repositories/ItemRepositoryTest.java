package com.mendix.assignment.backend.repositories;

import com.mendix.assignment.backend.dtos.ItemDto;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.List;


public class ItemRepositoryTest extends RepositoryTestLoader {

    /*
    *  Repositories are overrided from JPARepository and they are already tested
    *  This Test class ensure that the data loading of all items in items.json file is done successfully and the constraints of the application are not violated
    */

    @Test
    public void FindAllItems() {
        List<ItemDto> items = itemRepository.findAll();
        Assertions.assertEquals(18, items.size());
    }

    @Test
    public void getFirstItem() {
        ItemDto item = itemRepository.findById(1);
        Assertions.assertEquals("Pavilion 1",item.getName());
    }

    @Test
    public void getLastItem() {
        ItemDto item = itemRepository.findById(18);
        Assertions.assertEquals("Avocado",item.getName());
    }


}
