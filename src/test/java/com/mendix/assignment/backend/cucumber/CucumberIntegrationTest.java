package com.mendix.assignment.backend.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.spring.CucumberContextConfiguration;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;


/***
 * Running class for the cucumber tests
 */

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features/authentication")
@CucumberContextConfiguration
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@ContextConfiguration
public class CucumberIntegrationTest {

    private final String SERVER_URL = "http://localhost";
    private final String ENDPOINT = "/mendix/hiring_process/api/";

    @Autowired
    protected RestTemplate restTemplate;

    public String getENDPOINT() {
        return SERVER_URL+":"+port+ENDPOINT;
    }

    @LocalServerPort
    private int port;






}