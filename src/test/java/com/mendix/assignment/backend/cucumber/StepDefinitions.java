package com.mendix.assignment.backend.cucumber;

import com.mendix.assignment.backend.dtos.authentication.LoginRequest;
import com.mendix.assignment.backend.dtos.authentication.SignupRequest;
import com.mendix.assignment.backend.exception.LogInException;
import com.mendix.assignment.backend.mappers.User.UserResponse;
import com.mendix.assignment.backend.testentities.UserList;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Arrays;

/**
 * the main class for cucumber tests it includes the definition of every Step in the Gherkin Features
 */
public class StepDefinitions extends CucumberIntegrationTest {

    LoginRequest loginRequest;
    ResponseEntity<?> latestResponse;
    SignupRequest signupRequest;


    @Given("I have valid credentials")
    public void validCredentials() throws Exception{
        loginRequest = new LoginRequest("Mohamed","Bayoudh");
    }


    @When("I sign in")
    public void I_SIGN_IN() throws LogInException {
        latestResponse = restTemplate.postForEntity(getENDPOINT()+"/auth/signin", loginRequest, UserResponse.class);
    }

    @Then("my user information are returned")
    public void myUserInformationAreReturned() throws Exception{
        Assert.assertEquals(((UserResponse) latestResponse.getBody()).getEmail(),"Mohamed@gmail.com");
    }

    @Given("I have non-valid credentials")
    public void iHaveNonValidCredentials() throws Exception{
        loginRequest = new LoginRequest("Mohamed","Baoudh");
    }

    @Then("my user information are not returned and error message is returned with the information that access denied because of non-valid credentials")
    public void myUserInformationAreNotReturnedAndErrorMessageIsReturnedWithTheInformationThatAccessDeniedBecauseOfNonValidCredentials() throws Exception {
        Assertions.assertThrows(NullPointerException.class, () -> {
            Assert.assertEquals(((UserResponse) latestResponse.getBody()).getEmail(),"");
        });
    }

    @When("I sign in with non-valid credentials")
    public void iSignInWithNonValidCredentials() {
        Assertions.assertThrows(HttpClientErrorException.class, () -> {
            latestResponse = restTemplate.postForEntity(getENDPOINT()+"/auth/signin", loginRequest, UserResponse.class);
        });

    }

    @When("I sign out")
    public void iSignOut() {
        latestResponse = restTemplate.postForEntity(getENDPOINT()+"/auth/signout","", UserResponse.class);
    }

    @Given("I am signed in")
    public void iAmSignedIn() throws Exception {
        validCredentials();
        I_SIGN_IN();
    }

    @Then("I get Logout message")
    public void iGetLogoutMessage() {
        Assert.assertEquals(302,latestResponse.getStatusCode().value());
    }

    @Given("I have valid register credentials")
    public void iHaveValidRegisterCredentials() {
        signupRequest = new SignupRequest("newUser","newUser@email.com","newUserB");
    }

    @When("I sign up with valid credentials")
    public void iSignUpValid() {
        latestResponse = restTemplate.postForEntity(getENDPOINT()+"/auth/signup",signupRequest, UserResponse.class);
    }

    @When("I sign up with invalid credentials")
    public void iSignUpInvalid() {
        Assertions.assertThrows(HttpClientErrorException.class, () -> {
            latestResponse = restTemplate.postForEntity(getENDPOINT() + "/auth/signup", signupRequest, UserResponse.class);
        });
    }

    @Then("new user will be created with the given credentials")
    public void newUserWillBeCreatedWithTheGivenCredentials() {
        Assert.assertEquals("newUser@email.com",((UserResponse) latestResponse.getBody()).getEmail());
    }

    @Given("I try to sign up using already taken username")
    public void iTryToSignUpUsingAlreadyTakenUsername() {
        signupRequest = new SignupRequest("Mohamed","newUser@email.com","newUserB");
    }

    @Then("new user not created")
    public void newUserNotCreated() {
        Assert.assertEquals(latestResponse,null);
    }

    @Given("I try to sign up using already taken email")
    public void iTryToSignUpUsingAlreadyTakenEmail() {
        signupRequest = new SignupRequest("MohamedB","Mohamed@gmail.com","newUserB");
    }

    @When("I try to get all users as admin")
    public void iTryToGetAllUsersAdmin() throws  Exception{
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        //latestResponse = restTemplate.getForEntity(getENDPOINT() + "/users/getUsers", UserList.class);
        latestResponse  = restTemplate.exchange(getENDPOINT() + "/users/getUsers", HttpMethod.GET,entity ,ResponseEntity.class);
    }

    @When("I try to get all users as user")
    public void iTryToGetAllUsersUser() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Accept", "*/*");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        Assertions.assertThrows(HttpClientErrorException.class, () -> {
           // latestResponse = restTemplate.getForEntity(getENDPOINT() + "/users/getUsers", entity ,ResponseEntity.class);
            latestResponse  = restTemplate.exchange(getENDPOINT() + "/users/getUsers", HttpMethod.GET,entity ,ResponseEntity.class);
        });

    }

    @Then("all users are retrieved")
    public void allUsersAreRetrieved() {
        Assert.assertEquals(5, ((UserList)latestResponse.getBody()).getUsers().size());
    }

    @Then("Error message is returned with the information that action forbidden")
    public void errorMessageIsReturnedWithTheInformationThatActionForbidden() {
        Assert.assertEquals(null,latestResponse);
    }

    @Given("I am signed in as an Admin")
    public void iAmSignedInAsAnAdmin() {
        loginRequest = new LoginRequest("Mohamed","Bayoudh");
        latestResponse = restTemplate.postForEntity(getENDPOINT()+"/auth/signin", loginRequest, UserResponse.class);
    }

    @Given("I am signed in as a user")
    public void iAmSignedInAsAUser() {
        loginRequest = new LoginRequest("Patrick","Hepp");
        latestResponse = restTemplate.postForEntity(getENDPOINT()+"/auth/signin", loginRequest, UserResponse.class);
    }
}