package com.mendix.assignment.backend.controllers;

import com.mendix.assignment.backend.mappers.Item.ItemRequest;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



/***
 * jUnit Tests for the ItemController, most of the cases are tested without involving the main buisness logic of the API
 */


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ItemControllerTest extends ControllerTestLoader {


    String controllerPath = "/items";

    @Test
    public void AllItemsTest() throws Exception {
        setMVC();
        mockMvc.perform( MockMvcRequestBuilders
                        .get(controllerPath+"/getAllItems")
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value("18"));
    }

    @Test
    public void getItemsPerSetTest() throws Exception {
        setMVC();
        ArrayList<Long> list = new ArrayList<>();
        list.add((long) 2);
        list.add((long) 12);
        list.add((long) 14);
        mockMvc.perform( MockMvcRequestBuilders
                        .post(controllerPath+"/getItemsPerSet")
                        .content(asJsonString(list))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(3));
    }


    @Test
    public void getItemsPerSetTest2() throws Exception {
        setMVC();
        ArrayList<Long> list = new ArrayList<>();
        list.add((long) 2);
        list.add((long) 12);
        list.add((long) 44);
        mockMvc.perform( MockMvcRequestBuilders
                        .post(controllerPath+"/getItemsPerSet")
                        .content(asJsonString(list))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.ALL))
                .andExpect(status().isNotFound());
    }



    @Test
    public void insertItemTest() throws Exception {
        setMVC();
        ItemRequest item = new ItemRequest();
        ArrayList<String> list = new ArrayList<>();
        list.add("food");
        item.setName("Bread");
        item.setCategories(list);
        item.setDescription("Vollkorn Bread");
        item.setPrice(1.49);
        mockMvc.perform( MockMvcRequestBuilders
                        .post(controllerPath+"/addItemInDb")
                        .content(asJsonString(item))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(19));
    }


    @Test
    public void deleteItemTest() throws Exception {
        setMVC();
        mockMvc.perform( MockMvcRequestBuilders
                        .delete(controllerPath+"/deleteItemFromDb/15")
                        .accept(MediaType.ALL))
                .andExpect(status().isOk());

        ArrayList<Long> list = new ArrayList<>();
        list.add((long) 15);
        mockMvc.perform( MockMvcRequestBuilders
                        .post(controllerPath+"/getItemsPerSet")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(list))
                        .accept(MediaType.ALL))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getItemsPerCategoriesTest() throws Exception {
        setMVC();
        mockMvc.perform( MockMvcRequestBuilders
                        .get(controllerPath+"/getItemsPerCategories")
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(15));
    }

    @Test
    public void getItemsPerCategoryTest() throws Exception {
        setMVC();
        mockMvc.perform( MockMvcRequestBuilders
                        .get(controllerPath+"/getItemsPerCategory/electronics")
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(7));
    }

}
