package com.mendix.assignment.backend.controllers;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



/***
 * jUnit Tests for the user controller, most of the cases are tested without involving the main buisness logic of the API
 */


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserControllerTest extends ControllerTestLoader {

    String controllerPath = "/users";
    @Test
    public void upgradeUserTest() throws Exception {
        setMVC();
        mockMvc.perform( MockMvcRequestBuilders
                        .post(controllerPath+"/userAsAdmin/1")
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.role").value("admin"));
    }


    @Test
    public void UserNotFoundTest() throws Exception {
        setMVC();
        mockMvc.perform( MockMvcRequestBuilders
                        .post(controllerPath+"/userAsAdmin/7")
                        .accept(MediaType.ALL))
                .andExpect(status().isNotFound());
    }


    @Test
    public void getUsersTest() throws Exception  {
        setMVC();
        mockMvc.perform( MockMvcRequestBuilders
                        .get(controllerPath+"/getUsers")
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(3));
    }


    @Test
    public void getUserByUsernameTest() throws Exception {
        setMVC();
        mockMvc.perform( MockMvcRequestBuilders
                        .get(controllerPath+"/username/Joshua")
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(4));
    }

    @Test
    public void getUserByIdTest() throws Exception {
        setMVC();
        mockMvc.perform( MockMvcRequestBuilders
                        .get(controllerPath+"/id/1")
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("Lara@gmail.com"));
    }


    @Test
    public void deleteUserByUsernameTest() throws Exception {
        setMVC();
        mockMvc.perform( MockMvcRequestBuilders
                        .delete(controllerPath+"/deleteById/3")
                        .accept(MediaType.ALL))
                .andExpect(status().isOk());

        mockMvc.perform( MockMvcRequestBuilders
                        .get(controllerPath+"/id/3")
                        .accept(MediaType.ALL))
                .andExpect(status().isNotFound());
    }

}
