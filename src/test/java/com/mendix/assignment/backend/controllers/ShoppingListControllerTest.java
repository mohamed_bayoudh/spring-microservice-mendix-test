package com.mendix.assignment.backend.controllers;

import com.mendix.assignment.backend.dtos.ShoppingListDto;
import com.mendix.assignment.backend.mappers.ShoppingList.ShoppingListRequest;
import com.mendix.assignment.backend.repositories.ShoppingListRepository;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/***
 * jUnit Tests for the shopping list controller, most of the cases are tested without involving the main buisness logic of the API
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ShoppingListControllerTest extends ControllerTestLoader {

    String controllerPath = "/shoppingList";

    @Mock
    @Autowired
    private ShoppingListRepository shoppingListRepository;

    @Test
    public void Test1findShoppingListById() throws Exception {
        setMVC();
        ArrayList<Long> list = getSimpleList();
        ShoppingListDto shoppingListDto1 =
                new ShoppingListDto("MockedShoppingList1", 2,
                        "Mocked shopping list for test issues", "Mocks", list);
        shoppingListRepository.save(shoppingListDto1);

        mockMvc.perform( MockMvcRequestBuilders
                        .get(controllerPath+"/findById/2")
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.itemList.length()").value("3"));

        shoppingListRepository.deleteAll();
    }



    @Test
    public void Test2findShoppingListsByActualUser() throws Exception {
        setMVC();
        ArrayList<Long> list = getSimpleList();
        ShoppingListDto shoppingListDto1 =
                new ShoppingListDto("MockedShoppingList1", 2,
                        "Mocked shopping list for test issues", "Mocks", list);
        ShoppingListDto shoppingListDto2 =
                new ShoppingListDto("MockedShoppingList2", 2,
                        "Mocked shopping list for test issues", "Mocks", list);

        shoppingListRepository.save(shoppingListDto1);
        shoppingListRepository.save(shoppingListDto2);

        mockMvc.perform( MockMvcRequestBuilders
                        .get(controllerPath+"/findByActualUser")
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value("2"));
        shoppingListRepository.deleteAll();
    }



    @Test
    public void Test3findShoppingListByTitle() throws Exception{
        setMVC();
        ArrayList<Long> list = getSimpleList();
        ShoppingListDto shoppingListDto1 =
                new ShoppingListDto("MockedShoppingList1", 2,
                        "Mocked shopping list for test issues", "Mocks", list);
        shoppingListRepository.save(shoppingListDto1);
        mockMvc.perform( MockMvcRequestBuilders
                        .get(controllerPath+"/findByTitle/MockedShoppingList1")
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.category").value("Mocks"));
        shoppingListRepository.deleteAll();
    }



    @Test
    public void Test4UpdateShoppingListTest() throws Exception{
        setMVC();
        ArrayList<Long> list = getSimpleList();
        ShoppingListDto shoppingListDtoFirst =
                new ShoppingListDto("MockedShoppingList1", 2,
                        "Mocked shopping list for test issues", "Mocks", list);
        ShoppingListDto shoppingListDto1 =
                new ShoppingListDto("The original shopping list", 2,
                        "Mocked shopping list for test issues", "Mocks", list);
        shoppingListRepository.save(shoppingListDtoFirst);
        shoppingListRepository.save(shoppingListDto1);
        ShoppingListRequest shoppingListRequest = new ShoppingListRequest("The new shopping list",
                "Mocked shopping list for test issues", "Mocks", list);

        int i = shoppingListRepository.findAll().size();
        if(i != 2) throw new RuntimeException("Size "+i);
        mockMvc.perform( MockMvcRequestBuilders
                        .post(controllerPath+"/update/6")
                        .content(asJsonString(shoppingListRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value("The new shopping list"));

        shoppingListRepository.deleteAll();
    }

    @Test
    public void Test5addItemToShoppingList() throws Exception {
        setMVC();
        ArrayList<Long> list = getSimpleList();
        ShoppingListDto shoppingListDto =
                new ShoppingListDto("MockedShoppingList", 2,
                        "Mocked shopping list for test issues", "Mocks", list);
        shoppingListRepository.save(shoppingListDto);
        mockMvc.perform( MockMvcRequestBuilders
                        .post(controllerPath+"/addItem/18/8")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.itemList.length()").value("4"));
        shoppingListRepository.deleteAll();
    }


    @Test
    public void Test6deleteItemFromShoppingList() throws Exception {
        setMVC();
        ArrayList<Long> list = getSimpleList();
        ShoppingListDto shoppingListDto =
                new ShoppingListDto("MockedShoppingList", 2,
                        "Mocked shopping list for test issues", "Mocks", list);
        shoppingListRepository.save(shoppingListDto);
        mockMvc.perform( MockMvcRequestBuilders
                        .post(controllerPath+"/deleteItem/3/9")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.itemList.length()").value("2"));
        shoppingListRepository.deleteAll();
    }


    @Test
    public void Test7deleteShoppingList() throws Exception {
        setMVC();
        ArrayList<Long> list = getSimpleList();
        ShoppingListDto shoppingListDto =
                new ShoppingListDto("MockedShoppingList", 2,
                        "Mocked shopping list for test issues", "Mocks", list);
        shoppingListRepository.save(shoppingListDto);
        mockMvc.perform( MockMvcRequestBuilders
                        .delete(controllerPath+"/delete/10")
                        .accept(MediaType.ALL))
                .andExpect(status().isOk());
        shoppingListRepository.deleteAll();
    }


    @Test
    public void Test8updateShoppingListTitle() throws Exception {
        setMVC();
        ArrayList<Long> list = getSimpleList();
        ShoppingListDto shoppingListDto =
                new ShoppingListDto("MockedShoppingList", 2,
                        "Mocked shopping list for test issues", "Mocks", list);
        shoppingListRepository.save(shoppingListDto);
        mockMvc.perform( MockMvcRequestBuilders
                        .post(controllerPath+"/updateTitle/11")
                        .content("New Title")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value("New Title"));
        shoppingListRepository.deleteAll();
    }

    @Test
    public void Test91updateShoppingListDescription() throws Exception {
        setMVC();
        ArrayList<Long> list = getSimpleList();
        ShoppingListDto shoppingListDto =
                new ShoppingListDto("MockedShoppingList", 2,
                        "Mocked shopping list for test issues", "Mocks", list);
        shoppingListRepository.save(shoppingListDto);
        mockMvc.perform( MockMvcRequestBuilders
                        .post(controllerPath+"/updateDescription/12")
                        .content("New Description")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").value("New Description"));
        shoppingListRepository.deleteAll();
    }


    @Test
    public void Test92findAllShoppingLists() throws Exception {
        setMVC();
        ArrayList<Long> list = getSimpleList();
        ShoppingListDto shoppingListDto1 =
                new ShoppingListDto("MockedShoppingList1", 2,
                        "Mocked shopping list for test issues", "Mocks", list);

        ShoppingListDto shoppingListDto2 =
                new ShoppingListDto("MockedShoppingList2", 2,
                        "Mocked shopping list for test issues", "Mocks", list);
        ShoppingListDto shoppingListDto3 =
                new ShoppingListDto("MockedShoppingList3", 2,
                        "Mocked shopping list for test issues", "Mocks", list);

        shoppingListRepository.save(shoppingListDto1);
        shoppingListRepository.save(shoppingListDto2);
        shoppingListRepository.save(shoppingListDto3);


        mockMvc.perform( MockMvcRequestBuilders
                        .get(controllerPath+"/getShoppingLists")
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value("3"));
        shoppingListRepository.deleteAll();
    }

    @Test
    public void Test93CreateShoppingList() throws Exception{
        setMVC();
        ArrayList<Long> list = getSimpleList();
        ShoppingListRequest shoppingListRequest = new ShoppingListRequest("MockedshoppingListRequest1",
                "Mocked shopping list for test issues", "Mocks", list);
        mockMvc.perform( MockMvcRequestBuilders
                        .post(controllerPath+"/create")
                        .content(asJsonString(shoppingListRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value("MockedshoppingListRequest1"));
        shoppingListRepository.deleteAll();
    }

    private ArrayList<Long> getSimpleList() {
        ArrayList<Long> list = new ArrayList<>();
        list.add((long) 1);
        list.add((long) 2);
        list.add((long) 3);
        return list;
    }
}
