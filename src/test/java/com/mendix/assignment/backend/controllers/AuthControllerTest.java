package com.mendix.assignment.backend.controllers;


import com.mendix.assignment.backend.dtos.authentication.LoginRequest;
import com.mendix.assignment.backend.dtos.authentication.SignupRequest;
import com.mendix.assignment.backend.repositories.UserRepository;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.SharedHttpSessionConfigurer.sharedHttpSession;


public class AuthControllerTest extends ControllerTestLoader {

    String controllerPath = "/auth";

    @Mock
    @Autowired
    UserRepository userRepository;

    // ************************************* SignOut Test *****************************************

    @Test
    public void logout() throws Exception {
        mockMvc.perform( MockMvcRequestBuilders
                        .post(controllerPath +"/signout")
                        .accept(MediaType.ALL))
                .andExpect(redirectedUrl("/auth/logoutSuccess"));
    }

    // ************************************* Registration Test *************************************

    @Test
    public void registerUser()  throws Exception {
        setMVCForAuthTest();
        SignupRequest signUpRequest = new SignupRequest("NewUser","newuseremail@new.com","NewUserPassword");
        mockMvc.perform( MockMvcRequestBuilders
                        .post(controllerPath+"/signup")
                        .content(asJsonString(signUpRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("5"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("newuseremail@new.com"));
        userRepository.deleteById(userRepository.findByUsername("NewUser").getId());
    }

    // ************************************* Authentication Test *************************************
    @Test
    public void authenticateUser() throws Exception{
        setMVCForAuthTest();
        mockMvc.perform( MockMvcRequestBuilders
                        .post(controllerPath+"/signin")
                        .content(asJsonString(new LoginRequest("Mohamed","Bayoudh")))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("2"));
    }

    // ***************************** Setting The Mock Environment for the tests ********************************
    public void setMVCForAuthTest() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(sharedHttpSession())
                .build();
    }

}
