package com.mendix.assignment.backend.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mendix.assignment.backend.dtos.authentication.LoginRequest;
import com.mendix.assignment.backend.repositories.ItemRepository;
import com.mendix.assignment.backend.repositories.ShoppingListRepository;
import com.mendix.assignment.backend.repositories.UserRepository;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.SharedHttpSessionConfigurer.sharedHttpSession;

/***
 * base-class for running controller tests
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class ControllerTestLoader {

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected WebApplicationContext webApplicationContext;

    // Enables the perservation of the authenticated Session since @WithMockUser is not working (causes Problems in the cast from UserDetails -> UserDetailsImp)
    // @BeforeEach is also not working (Jupiter Package)
    public void setMVC(){

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(sharedHttpSession())
                .build();
        try {
            mockMvc.perform( MockMvcRequestBuilders
                            .post("/auth/signin").content(asJsonString(new LoginRequest("Mohamed","Bayoudh")))
                            .contentType(MediaType.APPLICATION_JSON)
                            .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //************************************* Jackson-bind Bean *************************************
    public static String asJsonString(Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
