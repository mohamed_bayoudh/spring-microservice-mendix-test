package com.mendix.assignment.backend.controllers;

import com.mendix.assignment.backend.dtos.authentication.LoginRequest;
import com.mendix.assignment.backend.dtos.authentication.SignupRequest;
import com.mendix.assignment.backend.mappers.ShoppingList.ShoppingListRequest;
import com.mendix.assignment.backend.repositories.UserRepository;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.SharedHttpSessionConfigurer.sharedHttpSession;

public class IntegrationTests extends ControllerTestLoader {

    @Mock
    @Autowired
    private UserRepository userRepository;


    /***
     *
     * First Integration test is to test more the interaction and the robustness of the auth spring security
     * driven controller, the test simulates different cases of signin/out/up of different users created during the
     * tests or before
     *
     */



    @Test
    public void userSignInSignOutAndUpgradingRoleStory() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(sharedHttpSession())
                .build();
        //Mohamed makes a sign in
        mockMvc.perform( MockMvcRequestBuilders
                        .post("/auth/signin")
                        .content(asJsonString(new LoginRequest("Mohamed","Bayoudh")))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        //confirm that his identity could be retrieved by the FE
        mockMvc.perform( MockMvcRequestBuilders
                        .get("/users/getActualUser")
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value("Mohamed"));

        //Mohamed upgrades Lara and give her admin rights
        mockMvc.perform( MockMvcRequestBuilders
                        .post("/users/userAsAdmin/1")
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.role").value("admin"));
        //Mohamed sign out
        mockMvc.perform( MockMvcRequestBuilders
                        .post("/auth/signout")
                        .accept(MediaType.ALL))
                .andExpect(redirectedUrl(null));


        // Lara signs in
        mockMvc.perform( MockMvcRequestBuilders
                        .post("/auth/signin")
                        .content(asJsonString(new LoginRequest("LaraBeerenburg","ITLARA")))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.role").value("admin"));

        // Confirmation requested by the client
        mockMvc.perform( MockMvcRequestBuilders
                        .get("/users/getActualUser")
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value("LaraBeerenburg"));

        //Also Lara wants to grant Admin rights for other users
        mockMvc.perform( MockMvcRequestBuilders
                        .post("/users/userAsAdmin/4")
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.role").value("admin"));

        //Joshua signs in
        mockMvc.perform( MockMvcRequestBuilders
                        .post("/auth/signin")
                        .content(asJsonString(new LoginRequest("Joshua","Werkis")))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.role").value("admin"));

        mockMvc.perform( MockMvcRequestBuilders
                        .get("/users/getActualUser")
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value("Joshua"));


        mockMvc.perform( MockMvcRequestBuilders
                        .post("/auth/signout")
                        .accept(MediaType.ALL))
                .andExpect(redirectedUrl(null));

    }


    /***
     *
     * Second Integration test is to test more the main buisness logic of the microservice:
     *          1- User signs up
     *          2- User signs in
     *          3- User collect some items and bring them into a shopping list
     *          4- adding items into the shopping list
     *          5- deleting items from the shopping list
     *          6- User deletes the shopping list
     *
     */


    @Test
    public void userRegistrationAndShoppingListCreationStory() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(sharedHttpSession())
                .build();

        SignupRequest signUpRequest = new SignupRequest("Rohit","Rohit@new.com","Dakarol");
        mockMvc.perform( MockMvcRequestBuilders
                        .post("/auth/signup")
                        .content(asJsonString(signUpRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("6"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("Rohit@new.com"));

        mockMvc.perform( MockMvcRequestBuilders
                        .post("/auth/signin")
                        .content(asJsonString(new LoginRequest("Rohit","Dakarol")))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        ArrayList<Long> list = getSimpleList();
        ShoppingListRequest shoppingListRequest = new ShoppingListRequest("Healthy Food",
                "Some Healthy Food for grand-ma", "Mocks", list);


        mockMvc.perform( MockMvcRequestBuilders
                        .post("/shoppingList/create")
                        .content(asJsonString(shoppingListRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value("Healthy Food"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.userID").value(6));


        mockMvc.perform( MockMvcRequestBuilders
                        .post("/shoppingList/addItem/18/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.itemList.length()").value("4"));



        mockMvc.perform( MockMvcRequestBuilders
                        .post("/shoppingList/deleteItem/3/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.itemList.length()").value("3"));

        mockMvc.perform( MockMvcRequestBuilders
                        .post("/shoppingList/addItem/100/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.ALL))
                .andExpect(status().isNotFound());

        mockMvc.perform( MockMvcRequestBuilders
                        .post("/shoppingList/deleteItem/2/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.itemList.length()").value("2"));

        mockMvc.perform( MockMvcRequestBuilders
                        .delete("/shoppingList/delete/1")
                        .accept(MediaType.ALL))
                .andExpect(status().isOk());


        userRepository.deleteById((long)6);
    }




    private ArrayList<Long> getSimpleList() {
        ArrayList<Long> list = new ArrayList<>();
        list.add((long) 1);
        list.add((long) 2);
        list.add((long) 3);
        return list;
    }

}
