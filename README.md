# Spring Microservice - Mendix Test

This Microservice is Spring-based. It was developed under the context of the assignment for the hiring process of the developer in Mendix Company. 

You can clone the repository with the following commands in your local environment
```
cd existing_repo
git clone git@gitlab.com:mohamed_bayoudh/spring-microservice-mendix-test.git
```


![](documentation//Infrastructure.drawio.png)
![](documentation/Context-View.drawio.png)
![](documentation/microservice_pipeline.png)
![](documentation/docker-banner.png)


